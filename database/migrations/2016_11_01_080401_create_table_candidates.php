<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCandidates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('address');
            $table->integer('phone');
            $table->string('gender');
            $table->string('passport');
            $table->string('work_permit');
            $table->string('driving_license');
            $table->string('weewrc');
            $table->string('languages');
            $table->string('s_needs');
            $table->string('interview');
            $table->string('doi');
            $table->string('driving_code');
            $table->string('smokers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('candidates');
    }
}
