@extends('layouts.app')

@section('content')
<div class="container-fluid">
@if(Session::has('success'))
    <div class="alert alert-success">
        <a href="" data-dismiss="alert" class="close">&times;</a>
        {{Session::get('success')}}
    </div>
@endif
@if(Session::has('updated'))
    <div class="alert alert-success">
        <a href="" data-dismiss="alert" class="close">&times;</a>
        {{Session::get('updated')}}
    </div>
@endif
<a class="btn btn-success pull-right" href="{{URL::to('/employee/create')}}" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Create Employee</a>

    <table class="table table-hover table-bordered" >
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Address</th>
                <th>Phone</th>
                <th>DOB</th>
                <th>Gender</th>
                <th>EU Passwport</th>
                <th>Action</th>
                
            </tr>
        </thead>
        <tbody>
            <?php $i =0; ?>
            @foreach($candidates as $cad)
            <tr>
                <td>{{++$i}}</td>
                <td>{{$cad->name}}</td>
                <td>{{$cad->email}}</td>
                <td>{{$cad->address}}</td>
                <td>{{$cad->phone}}</td>
                <td>{{$cad->dob}}</td>
                <td>{{$cad->gender}}</td>
                <td>{{$cad->passport}}</td>
                <td>
                    <a href="{{URL::to('/employee/'.$cad->id)}}" class="btn btn-info btn-xs"> <i class="fa fa-eye" aria-hidden="true"></i>
                    Show</a>
                    <a href="{{URL::to('/employee/'.$cad->id.'/edit')}}" class="btn btn-warning btn-xs"> <i class="fa fa-pencil" aria-hidden="true"></i>
                    Edit</a>
                   {!! Form::open([ 'method'=>'DELETE', 'action'=>['EmployeeController@destroy',$cad->id], 'style'=>'display:inline']) !!}
        {!! Form::button('<i class="fa fa-trash"></i>    Delete',['type'=>'submit','class'=>'btn btn-danger btn-xs'])!!}
        {!! Form::close() !!}
                </td>
                
                
            </tr>
            @endforeach
        </tbody>
    </table>
  
</div>
@endsection
