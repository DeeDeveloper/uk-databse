@extends('layouts.app')

@section('content')

<div class="container">
<a href="{{URL::to('/employee')}}" class="btn btn-primary pull-right btn-xs"><i class="fa fa-chevron-left"></i> back</a>
<br>
<h3 class="text-center text-success">{{$candidate->email}} Details</h3>
<div class="thumbnail">
    <div class="row">
        <div class="col-md-6 col-xs-12 col-sm-6">
            <h3>Personal Information</h3>
                <div class="center-block">
                <p>
                    <strong>Name:</strong> <span class="label label-success">{{$candidate->name}}</span>
                    </p>

                    <p><strong>Phone:</strong><span class="label label-success">{{$candidate->phone}}</span> </p>
                     <p><strong>Email:</strong><span class="label label-success">{{$candidate->email}}</span> </p>
                      <p><strong>Address:</strong><span class="label label-success">{{$candidate->address}}</span></p>
                       <p><strong>DOB:</strong><span class="label label-success">{{$candidate->dob}}</span> </p>
                        <p><strong>Gender:</strong> <span class="label label-success">{{$candidate->gender}}</span></p>
                         <p><strong>Nationality:</strong> <span class="label label-success">{{$candidate->nationality}}</span></p>
                          <p><strong>Driving License No:</strong> <span class="label label-success">{{$candidate->driving_license}}</span></p>
                </div>
        </div>
        <div class="col-md-6 col-xs-12 col-sm-6">
            <h3>Other Information</h3>
                <div class="center-block">
                   
                    <p><strong>Driving License Issue Date:</strong> <span class="label label-info">{{$candidate->doi}}</span></p>
                    <p><strong>Work Permit:</strong> <span class="label label-info">{{$candidate->work_permit}}</span></p>
                    <p><strong>W.E.E.W.R.C:</strong> <span class="label label-info">{{$candidate->weewrc}}</span></p>
                    <p><strong>Languages:</strong> <span class="label label-info">{{$candidate->languages}}</span></p>
                    <p><strong>Experience Special Needs:</strong> <span class="label label-info">{{$candidate->s_needs}}</span></p>
                    <p><strong>Phone Interview:</strong> <span class="label label-info">{{$candidate->interview}}</span></p>
                    <p><strong>Smokers:</strong> <span class="label label-info">{{$candidate->smokers}}</span></p>
                    <p><strong>Issue Date:</strong> <span class="label label-info">{{$candidate->issue_date}}</span></p>
                </div>
        </div>
       
    </div>
</div>
</div>
@endsection
