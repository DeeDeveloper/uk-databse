@extends('layouts.app')

@section('content')
<div class="container">
<h3 class="text-center text-success">Create New Employee</h3>
<a href="{{URL::to('/employee')}}" class="btn btn-primary pull-right btn-xs"><i class="fa fa-chevron-left"></i> back</a>
<br>
<br>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
         Correct the following errors
        </ul>
    </div>
@endif
<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#education">Education</a></li>
  <li><a data-toggle="tab" href="#employment">Employment</a></li>
  
</ul>

<div class="tab-content">
  <div id="education" class="tab-pane fade in active">
    
  </div>
  <div id="employment" class="tab-pane fade">
    <div class="thumbnail">
    <div class="row">
    <div class="col-sm-10 col-sm-offset-1">
           {!! Form::open(['action'=>'EmployeeController@store']) !!}
   <div class="row">
       <div class="col-md-4 col-sm-6 col-xs-12">
        {!! Form::label('name','Full Names') !!}
        {!! Form::text('name',null,['class'=>'form-control']) !!} 
        <div class="help-block">
            {{$errors->first('name')}}
        </div>    
       </div>
       <div class="col-md-4 col-sm-6 col-xs-12">
           {!! Form::label('phone','Phone Number') !!}
        {!! Form::number('phone',null,['class'=>'form-control']) !!}
         <div class="help-block">
            {{$errors->first('phone')}}
        </div>
       </div>
       <div class="col-md-4 col-sm-6 col-xs-12">
           {!! Form::label('email','Email Address') !!}
        {!! Form::email('email',null,['class'=>'form-control']) !!}
        <div class="help-block">
            {{$errors->first('email')}}
        </div>
       </div>
   </div>
   <br>
   <p>
   <div class="row">
       <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="form-group">
        <label for="">Date of Birth</label>
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" name="dob" value="{{Old('dob')}}"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                <div class="help-block">
            {{$errors->first('dob')}}
        </div>
            </div>    
       </div>
       <div class="col-md-4 col-sm-6 col-xs-12">
           {!! Form::label('nationality','Nationality') !!}
        {!! Form::text('nationality',null,['class'=>'form-control']) !!} 
        <div class="help-block">
            {{$errors->first('nationality')}}
        </div>
       </div>
       <div class="col-md-4 col-sm-6 col-xs-12">
           {!! Form::label('wwewrc','WEEWRC') !!}
        {!!Form::select('weewrc', ['Yes' => 'Yes', 'No' => 'No'], null, ['placeholder' => 'Choose..','class'=>'form-control'])!!}
        <div class="help-block">
            {{$errors->first('weewrc')}}
        </div>
       </div>
   </div>
   </p>
   <p>
       
       <p><div class="row">
       <div class="col-md-4 col-sm-6 col-xs-12">
        {!! Form::label('address','Address') !!}
        {!! Form::text('address',null,['class'=>'form-control']) !!} 
        <div class="help-block">
            {{$errors->first('address')}}
        </div>
       </div>
       <div class="col-md-4 col-sm-6 col-xs-12">
           {!! Form::label('gender','Gender') !!}
        {!!Form::select('gender', ['Male' => 'Male', 'Female' => 'Female'], null, ['placeholder' => 'Gender','class'=>'form-control'])!!}
        <div class="help-block">
            {{$errors->first('gender')}}
        </div>
       </div>
       <div class="col-md-4 col-sm-6 col-xs-12">
           {!! Form::label('passport','EU Passport') !!}
        {!! Form::text('passport',null,['class'=>'form-control']) !!}
        <div class="help-block">
            {{$errors->first('passport')}}
        </div>
       </div>
   </div>
   </p>
   </br>
   <p>
   <div class="row">
       <div class="col-md-4 col-sm-6 col-xs-12">
        {!! Form::label('work_permit','Work Permit') !!}
        {!! Form::text('work_permit',null,['class'=>'form-control']) !!}  
        <div class="help-block">
            {{$errors->first('work_permit')}}
        </div>   
       </div>
       <div class="col-md-4 col-sm-6 col-xs-12">
           {!! Form::label('driving','Driving License') !!}
        {!! Form::text('driving_license',null,['class'=>'form-control']) !!} 
        <div class="help-block">
            {{$errors->first('driving_license')}}
        </div>
       </div>
       <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="form-group">
        <label for="">Date of Issue</label>
                <div class='input-group date' id='datetimepicker3'>
                    <input type='text' class="form-control" name="doi" value="{{Old('doi')}}" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                <div class="help-block">
            {{$errors->first('doi')}}
        </div>
            </div>
           
       </div>
   </div>
   </p>
   </br>
 
   <div class="row"> 
       <div class="col-md-4 col-sm-6 col-xs-12">
       {!! Form::label('special','Special Needs') !!}
        {!!Form::select('s_needs', ['Yes' => 'Yes', 'No' => 'No'], null, ['placeholder' => 'Special Needs','class'=>'form-control'])!!}
        <div class="help-block">
            {{$errors->first('s_needs')}}
        </div>
           
       </div>
       <div class="col-md-4 col-sm-6 col-xs-12">
       {!! Form::label('smokers','Smokers') !!}
        {!!Form::select('smokers', ['smokers' => 'Smokers', 'Vapour' => 'Vapour'], null, ['placeholder' => 'Choose..','class'=>'form-control'])!!}
        <div class="help-block">
            {{$errors->first('smokers')}}
        </div>
           
       </div>
       <div class="col-md-4 col-sm-6 col-xs-12">
       {!! Form::label('languages','Foreign Languages') !!}
        {!!Form::select('languages', ['Yes' => 'Yes', 'No' => 'No'], null, ['placeholder' => 'Languages','class'=>'form-control'])!!}
        <div class="help-block">
            {{$errors->first('languages')}}
        </div>
         
       </div>

   </div>
   </p>
   <br>
   <p>
   <div class="row">
              <div class="col-md-4 col-sm-6 col-xs-12">
       {!! Form::label('phone_interview','Phone Interview') !!}
        {!!Form::select('interview', ['Yes' => 'Yes', 'No' => 'No'], null, ['placeholder' => 'Select..','class'=>'form-control'])!!} 
        <div class="help-block">
            {{$errors->first('interview')}}
        </div>  
            
       </div>
       <div class="col-md-4 col-sm-6 col-xs-12">
       <div class="form-group">
        <label for="">Date of Issue</label>
                <div class='input-group date' id='datetimepicker2'>
                    <input type='text' class="form-control" name="issue_date" value="{{Old('issue_date')}}" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>

                </div>
                <div class="help-block">
            {{$errors->first('issue_date')}}
        </div>
            </div> 
          
       </div>
       <div class="col-md-4 col-sm-6 col-xs-12 ">
            {!! Form::button('<i class="fa fa-download"></i>    Register',['type'=>'submit','class'=>'btn btn-primary btn-block mybutton'])!!}
       
         
       </div>
       

   </div>
   </p>
   {!! Form::close() !!}

    </div>
</div>
</div>
  </div>
 
</div>



</div>
@endsection
@section('scripts')
<script>
$(document).ready(function(){
    $('#datetimepicker1').datetimepicker(); 
    $('#datetimepicker2').datetimepicker();
    $('#datetimepicker3').datetimepicker();        
 });
</script>
@endsection
