<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Candidate;
use Validator,Redirect;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $candidates = Candidate::all();
        return view('student.welcome',compact('candidates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('student.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
        'name'=>'required',
        'email'=>'required|email|unique:candidates',
        'address'=>'required',
        'phone'=>'integer|required',
        'gender'=>'required',
        'passport'=>'required',
        'driving_license'=>'required|unique:candidates',
        'weewrc'=>'required',
        'languages'=>'required',
        's_needs'=>'required',
        'work_permit'=>'required',
        'interview'=>'required',
        'doi'=>'required|date',
        'dob'=>'required|date',
        'issue_date'=>'required|date',
        'smokers'=>'required',
        'nationality'=>'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->passes()){
            //dd($request->input('dob'));
        $c = new Candidate();
        $c->name=$request->input('name');
        $c->email=$request->input('email');
        $c->address=$request->input('address');
        $c->phone=$request->input('phone');
        $c->gender=$request->input('gender');
        $c->driving_license=$request->input('driving_license');
        $c->passport=$request->input('passport');
        $c->weewrc=$request->input('weewrc');
        $c->languages=$request->input('languages');
        $c->s_needs=$request->input('s_needs');
        $c->work_permit=$request->input('work_permit');
        $c->interview=$request->input('interview');
        $c->doi=$request->input('doi');
        $c->dob=$request->input('dob');  
        $c->issue_date=$request->input('issue_date');
        $c->smokers=$request->input('smokers');
        $c->nationality=$request->input('nationality');
        $c->save();
         return  Redirect::to('/employee')->with('success','Record added successfully');
        }
         return  Redirect::to('/employee/create')->withErrors($validator)->withInput();
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $candidate = Candidate::find($id);
        return view('student.show',compact('candidate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $candidate = Candidate::find($id);
        return view('student.edit',compact('candidate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $rules = [
        // 'name'=>'required',
        // 'email'=>'required|email|unique:candidates',
        // 'address'=>'required',
        // 'phone'=>'integer|required',
        // 'gender'=>'required',
        // 'passport'=>'required',
        // 'driving_license'=>'required|unique:candidates',
        // 'weewrc'=>'required',
        // 'languages'=>'required',
        // 's_needs'=>'required',
        // 'work_permit'=>'required',
        // 'interview'=>'required',
        // 'doi'=>'required|date',
        // 'dob'=>'required|date',
        // 'issue_date'=>'required|date',
        // 'smokers'=>'required',
        // 'nationality'=>'required'
        // ];

        // $validator = Validator::make($request->all(), $rules);
        // if($validator->passes()){
            
        $c = Candidate::find($id);
        //dd($request->all())->toArray();
        $c->name=$request->input('name');
        $c->email=$request->input('email');
        $c->address=$request->input('address');
        $c->phone=$request->input('phone');
        $c->gender=$request->input('gender');
        $c->driving_license=$request->input('driving_license');
        $c->passport=$request->input('passport');
        $c->weewrc=$request->input('weewrc');
        $c->languages=$request->input('languages');
        $c->s_needs=$request->input('s_needs');
        $c->work_permit=$request->input('work_permit');
        $c->interview=$request->input('interview');
        $c->doi=$request->input('doi');
        $c->dob=$request->input('dob');  
        $c->issue_date=$request->input('issue_date');
        $c->smokers=$request->input('smokers');
        $c->nationality=$request->input('nationality');
        $c->save();
         return  Redirect::to('/employee')->with('updated','Record updated successfully');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $c = Candidate::find($id);
        $c->delete();
        return Redirect::to('/employee');
    }

    public function education()
    {
        # code...
    }
}
